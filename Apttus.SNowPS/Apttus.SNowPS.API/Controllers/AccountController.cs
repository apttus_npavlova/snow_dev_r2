﻿/****************************************************************************************
@Name: AccountController.cs
@Author: Mahesh Patel
@CreateDate: 1 Sep 2017
@Description: Resolves ExternalIDs and Upserts accounts using generic API 
@UsedBy: This will be used by Mule as an API call

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/

using Apttus.SNowPS.Common;
using Apttus.SNowPS.Model;
using Apttus.SNowPS.Respository;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.Hosting;
using System.Web.Script.Serialization;

namespace Apttus.SNowPS.API.Controllers
{
    [ServiceRequestActionFilter]
    public class AccountController : ApiController
    {
        private readonly AccountRepository _account = new AccountRepository();
        private string muleXClientId = ConfigurationManager.AppSettings[Constants.CONFIG_MULEXCLIENTID];
        private string muleXSourceSystem = ConfigurationManager.AppSettings[Constants.CONFIG_MULEXSOURCESYSTEM];
        private string _accessToken = null; 

        /// <summary>
        /// Advance Account Search
        /// </summary>
        /// <returns></returns>
        public HttpResponseMessage Get()
        {
            try
            {
                if (this.Request != null && this.Request.RequestUri != null && !string.IsNullOrEmpty(this.Request.RequestUri.Query))
                {
                    var headers = this.Request.Headers;
                    var accessToken = headers.Authorization != null ? Convert.ToString(headers.Authorization) : null;
                    var encodedQuery = this.Request.RequestUri.Query;
                    var decodedQuery = Utilities.GetDecodedQuery(encodedQuery);

                    var reqConfig = new RequestConfigModel();
                    reqConfig.accessToken = accessToken;
                    reqConfig.objectName = Constants.OBJ_ACCOUNT;

                    var response = Utilities.GetSearch(decodedQuery, reqConfig);

                    return Utilities.CreateResponse(response);
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, "");
            }
            catch (Exception ex)
            {
                //TODO
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Dictionary<string, object> { { Constants.STR_MESSAGE, ex.Message } });
            }
        }

        /// <summary>
        /// Resolves external ids and upserts accounts using standard API
        /// </summary>
        /// <param name="dictContent"></param>
        /// <returns></returns>
        [ActionName("Upsert")]
        [HttpPost]
        public HttpResponseMessage UpsertAccount([FromBody] List<Dictionary<string, object>> dictContent)
        {
            try
            {
                if (dictContent != null && dictContent.Count() > 0)
                {
                    //Get Access Token from Headers
                    var headers = this.Request.Headers;
                    _accessToken = headers.Authorization != null ? Convert.ToString(headers.Authorization) : null;

                    if (!string.IsNullOrEmpty(_accessToken))
                    {
                        var responseAccount = _account.UpsertAccount(dictContent, _accessToken);

                        //Create Primary Address
                        if (responseAccount != null && responseAccount.IsSuccessStatusCode && dictContent.Count > 0)
                        {
                            //Get Legal Address Data
                            var addressContent = new List<Dictionary<string, object>>();
                            var responseString = responseAccount.Content.ReadAsStringAsync().Result;
                            var responseAccObj = JsonConvert.DeserializeObject<BulkOperationResponseModel>(responseString);

                            _account.GetAddressData(dictContent, addressContent, responseAccObj);

                            //Create Primary Address
                            if (addressContent != null && addressContent.Count > 0)
                            {
                                var responseAddress = _account.UpsertPrimaryAddress(addressContent, _accessToken);

                                //Update PrimaryAddressIds in Account
                                if (responseAddress != null && responseAddress.IsSuccessStatusCode)
                                {
                                    //Fetch PrimaryAddressIds
                                    var primaryAddressContent = new List<Dictionary<string, object>>();
                                    responseString = responseAddress.Content.ReadAsStringAsync().Result;
                                    var responseAddrObj = JsonConvert.DeserializeObject<BulkOperationResponseModel>(responseString);
                                    _account.GetPrimaryAddresses(addressContent, primaryAddressContent, responseAddrObj);

                                    //Update Primary Address in Account
                                    if (primaryAddressContent != null && primaryAddressContent.Count > 0)
                                    {
                                        var responsePrimaryAddress = _account.UpdatePrimaryAddressId(primaryAddressContent, _accessToken);

                                        if (responsePrimaryAddress == null || !responsePrimaryAddress.IsSuccessStatusCode)
                                            return responsePrimaryAddress;
                                    }
                                }
                                else
                                {
                                    return responseAddress;
                                }
                            }
                        }

                        return responseAccount;
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, new Dictionary<string, object> { { Constants.STR_MESSAGE, Constants.ERR_AUTH_TOKEN_MISSING } });
                    }
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, "");
            }
            catch (Exception ex)
            {
                //TODO
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Dictionary<string, object> { { Constants.STR_MESSAGE, ex.Message } });
            }
        }

        /// <summary>
        /// Use for Account Sync 
        /// </summary>
        /// <param name="dictContent"></param>
        /// <param name="sendType"></param>
        /// <returns></returns>
        [ActionName("Sync")]
        [HttpPost]
        public HttpResponseMessage AccountSync([FromBody] Dictionary<string, object> dictContent)
        {
            try
            {
                List<Dictionary<string, object>> lstDictContent = new List<Dictionary<string, object>>();
                lstDictContent.Add(dictContent);
                var response = _account.SendAccountDataToMule(lstDictContent);

                if (response != null)
                    return response;
                else
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "");
            }
            catch (Exception ex)
            {
                //TODO
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Dictionary<string, object> { { Constants.STR_MESSAGE, ex.Message } });
            }
        }
        /// <summary>
        /// Use for Sync Key Contact Roles 
        /// </summary>
        /// <param name="dictContent"></param>
        /// <returns></returns>
        [ActionName("SyncKeyContactRoles")]
        [HttpPost]
        public HttpResponseMessage KeyContactRoleSync([FromBody] Dictionary<string, object> dictContent)
        {
            try
            {
                List<Dictionary<string, object>> lstDictContent = new List<Dictionary<string, object>>();
                lstDictContent.Add(dictContent);
                var response = _account.SendKeyContactRoleDataToMule(lstDictContent);
                if (response != null)
                    return response;
                else
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Message :" + ex.Message + " " + "StackStrace :" + ex.StackTrace);
            }
        }
    }
}
