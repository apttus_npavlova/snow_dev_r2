﻿/****************************************************************************************
@Name: AgreementController.cs
@Author: Kiran Satani
@CreateDate: 09 Sep 2017
@Description: Synch Agreement from Apttus to Mule
@UsedBy: This will be used by Mule as an API call

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/
using Apttus.SNowPS.Common;
using Apttus.SNowPS.Model;
using Apttus.SNowPS.Repository.Integration;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Script.Serialization;
using Apttus.SNowPS.Repository.CLM;
using CLMAgreementRepository = Apttus.SNowPS.Repository.CLM.AgreementRepository;
using Apttus.DataAccess.Common.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Apttus.SNowPS.API.Controllers
{
    [ServiceRequestActionFilter]
    [RoutePrefix("api/snowps/v1")]
    public class AgreementController : BaseController
    {
        private readonly Repository.Integration.AgreementRepository _agreement = new Repository.Integration.AgreementRepository();

        [ActionName("Sync")]
        [HttpPost]
        public HttpResponseMessage SyncToMule(Dictionary<string, object> dicContent)
        {
            try
            {
                HttpResponseMessage httpMuleResponseMessage = null;
                if (dicContent != null && dicContent.Count > 0)
                {
                    //Generate authentication token
                    var accessToken = Utilities.GetAuthToken();

					//Get case-insensitive 
					var dicAgreement = Utilities.GetCaseIgnoreSingleDictContent(dicContent);

					string Id = dicAgreement.ContainsKey(Constants.FIELD_ID) && dicAgreement[Constants.FIELD_ID] != null ? Convert.ToString(dicAgreement[Constants.FIELD_ID]) : null;

					//Get Product Json For Mule
					var agreementJsonForMule = _agreement.GetAgreementDetail(Id, accessToken);

                    //Call Mule API
                    if (!string.IsNullOrEmpty(agreementJsonForMule))
                    {
                        MuleHeaderModel objMuleHeaderModel = new MuleHeaderModel();
                        objMuleHeaderModel.APIName = ConfigurationManager.AppSettings[Constants.CONFIG_AGREEMENTSYNCAPI];
                        objMuleHeaderModel.MuleUrl = ConfigurationManager.AppSettings[Constants.CONFIG_MOCKAPIURL];
                        objMuleHeaderModel.XClientId = ConfigurationManager.AppSettings[Constants.CONFIG_MULEXCLIENTID];
                        objMuleHeaderModel.HttpContentStr = new StringContent(agreementJsonForMule, Encoding.UTF8, "application/json");
                        objMuleHeaderModel.AccessToken = Utilities.GetMuleAuthToken();
                        httpMuleResponseMessage = HttpActions.PostRequestMule(objMuleHeaderModel);
                        if (httpMuleResponseMessage.IsSuccessStatusCode)
                        {
                            var muleResponseDict = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(httpMuleResponseMessage.Content.ReadAsStringAsync().Result);
                            var ignoreCaseMuleResDict = Utilities.GetCaseIgnoreSingleDictContent(muleResponseDict);
                            if (ignoreCaseMuleResDict.ContainsKey(Constants.FIELD_ID) && ignoreCaseMuleResDict.ContainsKey(Constants.FIELD_CONSUMERID))
                            {
                                var id = ignoreCaseMuleResDict[Constants.FIELD_ID];
                                var externalId = ignoreCaseMuleResDict[Constants.FIELD_CONSUMERID];
                                var response = Utilities.UpdateMuleResponse(Convert.ToString(id), Convert.ToString(externalId), Constants.OBJ_AGREEMENT);
                                return Utilities.CreateResponse(response);
                            }
                            else
                                return httpMuleResponseMessage;
                        }
                        return httpMuleResponseMessage;
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, "Data Not Found In Apttus System");
                }
                else
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Please Pass Agreement Id");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Message :" + ex.Message + " " + "StackStrace :" + ex.StackTrace);
            }
        }

        /// <summary>
        /// Resolves external ids  using standard API
        /// </summary>
        /// <param name="dictContent"></param>
        /// <returns></returns>
        [ActionName("Upsert")]
        [HttpPost]
        public HttpResponseMessage Post([FromBody]List<Dictionary<string, object>> dictContent)
        {
            try
            {
                var headers = this.Request.Headers;
                var accessToken = headers.Authorization != null ? Convert.ToString(headers.Authorization) : null;
                if (!string.IsNullOrEmpty(accessToken))
                {
                    //Upsert Agreement
                    return _agreement.UpsertAgreement(dictContent, accessToken);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new Dictionary<string, object> { { Constants.STR_MESSAGE, Constants.ERR_AUTH_TOKEN_MISSING } });
                }
            }
            catch (Exception ex)
            {
                //TODO
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Dictionary<string, object> { { Constants.STR_MESSAGE, ex.Message } });
            }
        }

        /// <summary>
        /// This API submits the agreement to the Sales Ops Rep.
        /// Adds a note which is to be entered while submitting
        /// Changes the owner of the agreement to the Sales Ops Rep user
        /// </summary>
        /// <param name="id">agreement id</param>
        /// <param name="dictContent">note details</param>
        /// <returns>The id of the note added</returns>
        [HttpPost]
        public IHttpActionResult SubmitToSalesOps([FromUri]string id, [FromBody]List<Dictionary<string, object>> dictContent)
        {
            try
            {
                // Add note to the agreement with the provided details
                var notesManager = new NotesRepository(AccessToken);
                var response = notesManager.AddNote(dictContent, Constants.OBJ_AGREEMENT, id);

                var reason = dictContent[0]["reasonforsubmissiontosalesops"].ToString();

                // Assigns the agreement to Sales Ops rep
                var agreementManager = Repository.CLM.AgreementRepository.Instance(AccessToken);
                agreementManager.SubmitToSalesOps(id, reason);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpPost]
        [Route("CloneAndLinkOFinUA")]
        public string CloneAndLinkOFinUA([FromBody]Dictionary<string, object> body)
        {
            try
            {
                Guid id;
                if (body.ContainsKey("id") && Guid.TryParse(body["id"].ToString(), out id))
                {
                    /*Retrieve base agreement object*/

                    var query = new DataAccess.Common.Model.Query
                    {
                        EntityName = AgreementObjectConstants.ENTITYNAME,
                        Columns = { AgreementObjectConstants.RECORDTYPEID },
                        Criteria = new DataAccess.Common.Model.Expression
                        {
                            Conditions =
                            {
                                new DataAccess.Common.Model.Condition
                                {
                                    FieldName      = Constants.FIELD_ID,
                                    FilterOperator = DataAccess.Common.Enums.FilterOperator.Equal,
                                    Value          = id
                                }
                            }
                        }
                    };

                    var response = Utilities.Search(query.Serialize(), new Model.RequestConfigModel
                    {
                        accessToken = AccessToken,
                        objectName = Constants.OBJ_AGREEMENT,
                        searchType = Model.SearchType.AQL
                    });

                    if (response?.Content != null)
                    {

                    }

                    string newId = CLMAgreementRepository.Instance(AccessToken).Clone(id);
                    Guid newAgreementId;
                    if (Guid.TryParse(newId, out newAgreementId))
                    {
                    }
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [HttpPost]
        public IHttpActionResult CleanAgreementClausesDetails([FromBody]Dictionary<string, object> body)
        {
            try
            {
                Guid id;
                if (body.ContainsKey("id") && Guid.TryParse(body["id"].ToString(), out id))
                {
                    return Ok(CLMAgreementRepository.Instance(AccessToken).CleanAgreementClausesDetails(id));
                }
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpPost]
        public IHttpActionResult PopulatePdfTemplates([FromBody]Dictionary<string, object> body)
        {
            try
            {
                Guid agreementId;
                if (body.ContainsKey("Id") && Guid.TryParse(body["Id"].ToString(), out agreementId))
                {
                    return Ok(CLMAgreementRepository.Instance(AccessToken).PopulatePdfTemplates(agreementId));
                }
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpPost]
        [Route("UpdateAgreementFieldsFromQLI")]
        public IHttpActionResult UpdateAgreementFieldsFromQLI([FromBody]Dictionary<string, object> body)
        {
            try
            {
                Guid agreementId;
                if (body.ContainsKey("Id") && Guid.TryParse(body["Id"].ToString(), out agreementId))
                {
                    return Ok(CLMAgreementRepository.Instance(AccessToken).CalculatedAgreementFieldsFromQLI(new AgreementObjectModel { Id = agreementId.ToString() }, QuoteLineItemConstants.FIELDS));
                }
                return null;
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpPost]
        public IHttpActionResult CopyRelatedParticipantsFromQuote([FromBody]Dictionary<string, object> agreement)
        {
            try
            {
                if (agreement != null)
                {
                    return Ok(ParticipantRepository.Instance(AccessToken).CopyRelatedParticipantsFromQuote(agreement, new List<string> { ParticipantConstants.CONTEXTOBJECT, ParticipantConstants.EXTERNALID, ParticipantConstants.EXT_ISACTIVE, ParticipantConstants.EXT_JOBID, ParticipantConstants.EXT_JOBNAME, ParticipantConstants.PARTICIPANT }));
                }

                return null;
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }


        /// <summary>
        /// Sync Address for send to sap
        /// </summary>
        /// <param name="dictContent"></param>
        /// <returns></returns>
        [ActionName("SyncAgreementAddress")]
        [HttpPost]
        public HttpResponseMessage SyncAgreementAddress([FromBody]List<Dictionary<string, object>> dictContent)
        {
            try
            {
                var accessToken = Utilities.GetAuthToken();
                var response = _agreement.SyncAgreementAddress(dictContent, accessToken);
                if (response.IsSuccessStatusCode)
                    return Utilities.CreateResponse(response);
                else
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new Dictionary<string, object> { { Constants.STR_MESSAGE, response.Content.ReadAsStringAsync().Result } });
            }
            catch (Exception ex)
            {
                //TODO
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Dictionary<string, object> { { Constants.STR_MESSAGE, ex.Message } });
            }
        }
        /// Sets the default values for the list of fields mentioned in product setting value.
        /// </summary>
        /// <param name="body">The body.</param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult SetDefaultValues([FromBody]Dictionary<string, object> body)
        {
            try
            {
                Guid id;
                if (body.ContainsKey("id") && Guid.TryParse(body["id"].ToString(), out id))
                {
                    return Ok(CLMAgreementRepository.Instance(AccessToken).SetDefaultValues(id));
                }
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpPost]
        [ActionName("CopyFromSalesMasterAgreement")]
        public IHttpActionResult CopyFromSalesMasterAgreement([FromUri]string id)
        {
            try
            {
                Guid agreementId;
                if (Guid.TryParse(id, out agreementId))
                {
                    return Ok(CLMAgreementRepository.Instance(AccessToken).CopyFromSalesMasterAgreement(agreementId));
                }
                return null;
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        #region PS
        [ActionName("GetMilestonesByQuoteId")]
        [HttpPost]
        public HttpResponseMessage GetMilestonesByQuoteId(List<Dictionary<string, object>> dicQuote)
        {
            try
            {
                if (dicQuote != null && dicQuote.Count > 0)
                {
                    var headers = this.Request.Headers;
                    var accessToken = headers.Authorization != null ? Convert.ToString(headers.Authorization) : null;
                    var dicQuoteIngnoreCase = Utilities.GetCaseIgnoreDictContent(dicQuote);
                    string Id = Convert.ToString(dicQuoteIngnoreCase.Select(x => x[Constants.FIELD_ID]).FirstOrDefault());

                    if (accessToken == null)
                        accessToken = Utilities.GetAuthToken();

                    var quoteMilestoneJson = _agreement.GetMilestonesByQuoteId(Id, accessToken);
                    if (quoteMilestoneJson != null)
                    {
                        return Utilities.CreateResponse(quoteMilestoneJson);
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.NoContent, "No Record(s) Found");
                    }
                }
                else
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Please pass QuoteId");

            }
            catch (Exception ex)
            {
               return Request.CreateResponse(HttpStatusCode.BadRequest, "Message :" + ex.Message + " " + "StackStrace :" + ex.StackTrace);
            }
        }

        [HttpPost]
        public HttpResponseMessage PopulateApplications(Dictionary<string, object> dicAgreement)
        {
            try
            {
                List<Dictionary<string, object>> lstdicAgreement = new List<Dictionary<string, object>>();
                lstdicAgreement.Add(dicAgreement);

                if (lstdicAgreement == null)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.ERR_ARGUMENT_NULL);
                }
                string agreementId = Convert.ToString(lstdicAgreement.Select(x => x[Constants.FIELD_ID]).FirstOrDefault());
                string quoteNumber = Convert.ToString(lstdicAgreement.Select(x => x[Constants.FIELD_EXT_QUOTENUMBER]).FirstOrDefault());

                if (string.IsNullOrEmpty(quoteNumber) || string.IsNullOrWhiteSpace(quoteNumber))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, Constants.ERR_ARGUMENT_NULL);
                }

                var accessToken = Request?.Headers?.Authorization != null ? Convert.ToString(Request.Headers.Authorization) : Utilities.GetAuthToken();

                var reqConfig = new RequestConfigModel
                {
                    accessToken = accessToken,
                    objectName = Constants.OBJ_CPQ_QUOTELINEITEM,
                    searchType = SearchType.AQL
                };

                /*Query to get quote line items from QuoteNumber/ID*/
                var queryQuoteLineItem = new Query()
                {
                    EntityName = Constants.OBJ_CPQ_QUOTELINEITEM,
                    Columns = Constants.OBJ_FIELDS_POPULATEPRODUCTAPPLICATION.Split(Constants.CHAR_COMMA)?.ToList(),
                    Joins = new List<Join>()
                {
                    new Join
                    {
                        JoinType      = DataAccess.Common.Enums.JoinType.INNER,
                        EntityAlias   = Constants.OBJALIAS_QUOTE,
                        FromEntity    = Constants.OBJ_CPQ_QUOTELINEITEM,
                        FromAttribute = Constants.FIELD_QUOTEID,
                        ToEntity      = Constants.OBJ_QUOTE,
                        ToAttribute   = Constants.FIELD_ID,
                        JoinCriteria  = new Expression
                        {
                            Conditions = new List<Condition>()
                            {
                                new Condition
                                {
                                    EntityName = Constants.OBJ_QUOTE,
                                    FieldName = Constants.OBJALIAS_QUOTE+ Constants.CHAR_DOT + Constants.FIELD_EXT_QUOTENUMBER,
                                    FilterOperator = DataAccess.Common.Enums.FilterOperator.Equal,
                                    Value = quoteNumber
                                }
                            }
                        }
                    },
                    new Join
                    {
                        JoinType = DataAccess.Common.Enums.JoinType.INNER,
                        EntityAlias = Constants.OBJALIAS_PRODUCT,
                        FromEntity = Constants.OBJ_CPQ_QUOTELINEITEM,
                        FromAttribute = Constants.FIELD_PRODUCTID,
                        ToEntity = Constants.OBJ_PRODUCT,
                        ToAttribute = Constants.FIELD_ID,
                        JoinCriteria = new Expression
                        {
                            Conditions =  new List<Condition>()
                            {
                                new Condition
                                {
                                    EntityName = Constants.OBJ_PRODUCT,
                                    FieldName = Constants.OBJALIAS_PRODUCT + Constants.CHAR_DOT + Constants.FIELD_EXT_FAMILY1,
                                    FilterOperator = DataAccess.Common.Enums.FilterOperator.Equal,
                                    Value = Constants.SERVICES
                                }
                            }
                        }
                    },

                }
                };

                var response = Utilities.Search(queryQuoteLineItem.Serialize(), reqConfig);
                if (response != null && response.IsSuccessStatusCode)
                {
                    var responseString = JObject.Parse(response.Content.ReadAsStringAsync().Result).SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES).ToString();
                    var quoteLineItems = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(responseString);
                    if (quoteLineItems != null && quoteLineItems.Count > 0)
                    {
                        foreach (var item in quoteLineItems)
                        {
                            string productCategory = string.Empty;

                            var classificationId = item.ContainsKey("ClassificationId") && item["ClassificationId"] != null ? JsonConvert.DeserializeObject<Dictionary<string, object>>(item["ClassificationId"].ConvertToString()) : null;

                            if (classificationId != null)
                            {
                                productCategory = classificationId.ContainsKey(Constants.FIELD_NAME) ? classificationId[Constants.FIELD_NAME].ToString() : null;
                            }

                            string[] applications = { };

                            if (productCategory == Constants.EDUCATIONSERVICES || productCategory == Constants.FIXEDFEESERVICES)
                            {
                                string applicationFieldName = Constants.OBJALIAS_PRODUCT + Constants.CHAR_DOT + Constants.FIELD_EXT_Application;
                                applications = item.ContainsKey(applicationFieldName) ? item[applicationFieldName].ToString().Split(Constants.CHAR_COMMA) : null;
                            }
                            else if (productCategory == Constants.TAILOREDSERVICES)
                            {
                                var AttributeValueId = item.Keys.Contains("AttributeValueId") ? item["AttributeValueId"]?.ToString() : null;
                                if (AttributeValueId != null)
                                {
                                    var queryQPAV = new Query
                                    {
                                        EntityName = "cpq_QuoteProductAttributeValue",
                                        Columns = { "ext_ServicePackagesPS" },
                                        Criteria = new Expression
                                        {
                                            Conditions = new List<Condition>()
                                        {
                                            new Condition
                                            {
                                                FieldName = Constants.FIELD_ID,
                                                FilterOperator = DataAccess.Common.Enums.FilterOperator.Equal,
                                                Value = AttributeValueId
                                            }
                                        }
                                        }
                                    };
                                    var responseQPAVApi = Utilities.Search(queryQPAV.Serialize(), new RequestConfigModel
                                    {
                                        objectName = "cpq_QuoteProductAttributeValue",
                                        accessToken = accessToken,
                                        searchType = SearchType.AQL
                                    });
                                    var responseQPAV = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(JObject.Parse(responseQPAVApi.Content.ReadAsStringAsync().Result).
                                                                SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES).ToString());
                                    if (responseQPAV != null && responseQPAV.Count > 0 && responseQPAV.First().ContainsKey("ext_ServicePackagesPS"))
                                    {
                                        applications = responseQPAV.First()["ext_ServicePackagesPS"]?.ToString().Split(Constants.CHAR_COMMA);
                                    }
                                }
                            }
                            else
                            {
                                applications = null;
                            }

                            if (applications != null)
                            {
                                if (applications != null && applications.Length > 0)
                                {
                                    var quoteLineItemId = item.ContainsKey(Constants.FIELD_ID) ? item[Constants.FIELD_ID] : null;
                                    var quoteId = item.ContainsKey(Constants.FIELD_QUOTEID) ? item[Constants.FIELD_QUOTEID] : null;
                                    var productId = item.ContainsKey(Constants.FIELD_PRODUCTID) ? item[Constants.FIELD_PRODUCTID] : null;

                                    var servicesApplications = new List<dynamic>();

                                    foreach (var application in applications)
                                    {
                                        servicesApplications.Add(new
                                        {
                                            ext_AgreementId = agreementId,
                                            ext_QuoteId = quoteId,
                                            ext_ProductId = productId,
                                            ext_Application = application
                                        });
                                    }

                                    Utilities.Create(servicesApplications, new RequestConfigModel
                                    {
                                        accessToken = accessToken,
                                        objectName = Constants.OBJ_EXT_SERVICESAPPLICATION,
                                    });

                                    //Update quote line item with application data..
                                    var quoteLineItemResponse = Utilities.Update(new List<dynamic>() { new { Id = quoteLineItemId, ext_ApplicationPS = string.Join(Constants.CHAR_COMMA.ToString(), applications) } },
                                                                            new RequestConfigModel { accessToken = accessToken, objectName = Constants.OBJ_CPQ_QUOTELINEITEM });
                                }
                            }
                        }
                    }
                }
                
                return Request.CreateResponse(HttpStatusCode.OK, "Message : Success");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Message :" + ex.Message + " " + "StackStrace :" + ex.StackTrace);
            }
        }
        #endregion PS
    }

}

