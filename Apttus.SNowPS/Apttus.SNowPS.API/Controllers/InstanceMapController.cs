﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Apttus.SNowPS.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using Apttus.SNowPS.Common;
using Apttus.SNowPS.Respository;

namespace ServiceNow_CustomAPI.Controllers
{
    public class InstanceMapController : ApiController
    {      
        // GET: api/InstanceMap/5
        public HttpResponseMessage Get(string id)
        {
            #region //mockproducts
            //HttpResponseMessage respMsg = new HttpResponseMessage();
            //List<Instance> lstInst = new List<Instance>();

            //if (id == 1)
            //{
            //    for (int i = 0; i < 2; i++)
            //    {
            //        Instance inst = new Instance()
            //        {
            //            inst = "Prod Instance" + i,
            //            Products = MockProducts()
            //        };
            //        lstInst.Add(inst);
            //    }
            //}

            //else if (id == 2)
            //{
            //    for (int i = 0; i < 2; i++)
            //    {
            //        Instance inst = new Instance()
            //        {
            //            inst = "Add Prod Instance" + i,
            //            Products = MockProducts()
            //        };
            //        lstInst.Add(inst);
            //    }
            //}

            //else
            //{
            //    for (int i = 0; i < 3; i++)
            //    {
            //        Instance inst = new Instance()
            //        {
            //            inst = "Third Prod Instance" + i,
            //            Products = MockProducts()
            //        };
            //        lstInst.Add(inst);
            //    }
            //}
            //var json = new JavaScriptSerializer().Serialize(lstInst);
            //respMsg.Content = new StringContent(json);
            //return respMsg;

            #endregion

            HttpResponseMessage respMsg = new HttpResponseMessage();
            try
            {                
                var respContacts = string.Empty;
                var headers = this.Request.Headers;
                var accessToken = headers.Contains("Authorization") ? headers.GetValues("Authorization").First() : null;

                //Generate authentication token
                if (accessToken == null)
                    accessToken = Utilities.GetAuthToken();

                List<LineItem> lineItems = InstanceMappingRepository.GetLineItems(id, accessToken);

                //Build instances from the line items based on the Family1 value
                List<InstanceModel> instances = new List<InstanceModel>();
                int numberOfProdInstances = 0;
                lineItems.ForEach(l =>
                {
                    if (l.Product.Family1 != null)
                    {
                        if (l.Product.Family1.Key == Constants.MISC_PLATFORM)
                        {
                            if (l.Product.ExtFamily != null)
                            {
                                if (l.Product.ExtFamily.Key == Constants.PROD_INSTANCE || l.Product.ExtFamily.Key == Constants.ADDITIONAL_PROD_INSTANCE)
                                {
                                    numberOfProdInstances++;
                                }
                            }
                            instances.Add(new InstanceModel
                            {
                                inst = l.Product.Name,
                                Name = l.Name,
                                instName = l.InstanceName != null ? l.InstanceName : Constants.MISC_TBD,
                                instLIId = l.Id,
                                instLINumInstMap = l.LINumInstMap.ToString(),
                                IsProdInstance = l.Product.ExtFamily!=null ? (l.Product.ExtFamily.Key == Constants.PROD_INSTANCE || l.Product.ExtFamily.Key == Constants.ADDITIONAL_PROD_INSTANCE) : false,
                                SoldOnProducts = l.SoldLINameInstMap,
                                SoldOnProductsNumbers = l.SoldLINumInstMap,
                                InstalledOnProducts = l.InstalledLINameInstMap,
                                InstalledOnProductsNumbers = l.InstalledLINumInstMap,
                                Products = new List<InstanceProduct>()
                            }
                            );
                        }
                    }
                });

                //Map the products for each instance based on Family1 and Linetype
                instances.ForEach(i =>
                {
                    List<LineItem> filteredQLIItems = new List<LineItem>();

                    lineItems.Where(q=> q.Product.Family1.Key!= Constants.MISC_PLATFORM && q.LineType == Constants.MISC_PRODUCT_SERVICE).ToList().ForEach(p =>
                    {
                        i.Products.Add(new InstanceProduct
                        {
                            Id = p.Product.Id,
                            Name = p.Product.Name,
                            prodLIName = p.Name,
                            prodLIId = p.Id,
                            prodLINumInstMap = p.LINumInstMap.ToString(),
                            SoldOn = p.SoldLINameInstMap != null ? (p.SoldLINameInstMap.Contains(i.instName)) : false,
                            IsSoldOnDisabled = (i.IsProdInstance && p.SoldLINameInstMap != null) ? !(p.SoldLINameInstMap.Contains(i.instName)) : false,
                            //IsSoldOnDisabled = !((p.SoldLINameInstMap != null && p.SoldLINameInstMap.Count > 0 ) ? (p.SoldLINameInstMap.Contains(i.instName)) : false),
                            InstalledOn = p.InstalledLINameInstMap != null ? (p.InstalledLINameInstMap.Contains(i.instName)) : false,
                            //IsInstalledOnDisabled = !((p.InstalledLINameInstMap != null && p.InstalledLINameInstMap.Count > 0) ? (p.InstalledLINameInstMap.Contains(i.instName)) : false),
                            IsInstalledOnDisabled = (i.IsProdInstance && p.InstalledLINameInstMap != null) ? !(p.InstalledLINameInstMap.Contains(i.instName)) : false,
                            instId = i.inst,
                            IsProductionInstance = i.IsProdInstance
                        });
                    }
                    );

                    //i.Products.OrderBy(p => p.Name);
                    i.Products.Sort((x, y) => x.Name.CompareTo(y.Name));
                }
                );
                
                if(numberOfProdInstances == 1)
                {
                    InstanceModel singleProdInstance = instances.Find(i => i.IsProdInstance);                    
                    if ((singleProdInstance.InstalledOnProductsNumbers==null || singleProdInstance.InstalledOnProductsNumbers.Count == 0) 
                        && (singleProdInstance.SoldOnProductsNumbers==null) || singleProdInstance.SoldOnProductsNumbers.Count == 0)
                    {
                        //set the soldOn and installedOn to true in order to map it as checked by default
                        singleProdInstance.Products.ForEach(p => {
                            p.SoldOn = true;
                            p.InstalledOn = true;
                        });
                        List<InstanceModel> listOfInstancesFirstTimeMap = new List<InstanceModel>();
                        listOfInstancesFirstTimeMap.Add(singleProdInstance);
                        List<Dictionary<string, object>> upsValues = InstanceMappingRepository.BuildSKUMappingtoUpdate(ref listOfInstancesFirstTimeMap);
                        singleProdInstance = listOfInstancesFirstTimeMap.FirstOrDefault();

                        var reqConfig = new RequestConfigModel();
                        reqConfig.accessToken = accessToken;
                        reqConfig.objectName = Constants.OBJ_CPQ_LINEITEM;
                        respMsg = Utilities.Upsert(upsValues, reqConfig, false);

                        var jsonMsg = new JavaScriptSerializer().Serialize(instances);
                        respMsg.Content = new StringContent(jsonMsg);
                    }
                }

                else if(numberOfProdInstances > 1)
                {
                    //if the sold on and installed on are already mapped on any of the prod instances, disabled the properties for other line items
                    InstanceModel firstProdInstance = instances.Find(i=> i.IsProdInstance && i.SoldOnProducts != null && i.InstalledOnProducts!= null);
                    if (firstProdInstance != null)
                    {
                        instances.Where(i => i.IsProdInstance && i.instLIId != firstProdInstance.instLIId).ToList().ForEach(d => {
                            d.Products.ForEach(p =>
                            {
                                if(firstProdInstance.SoldOnProducts.Contains(p.Name))
                                    p.IsSoldOnDisabled = true;
                                if (firstProdInstance.InstalledOnProducts.Contains(p.Name))
                                    p.IsInstalledOnDisabled = true;
                            });
                        });
                    }
                }

                var json = new JavaScriptSerializer().Serialize(instances);
                respMsg.Content = new StringContent(json);
                return respMsg;
            }
            catch (Exception ex)
            {                
                respMsg.Content = new StringContent(Constants.ERR_INSTANCE_GENERIC);
                respMsg.StatusCode = HttpStatusCode.InternalServerError;
                throw new HttpResponseException(respMsg);                
            }
        }
       
        // POST: api/InstanceMap
        //public string Post([FromBody]List<Instance> instances)
        public HttpResponseMessage Post([FromBody]List<InstanceModel> instances)
        {
            HttpResponseMessage jsonResp = new HttpResponseMessage();
            try
            {
                var respContacts = string.Empty;
                var headers = this.Request.Headers;
                var accessToken = headers.Contains("Authorization") ? headers.GetValues("Authorization").First() : null;

                //Generate authentication token
                if (accessToken == null)
                    accessToken = Utilities.GetAuthToken();
                //instance name validation                
                List<CustomerInstanceModel> custInstances = InstanceMappingRepository.GetCustomerInstances(accessToken);
                instances.ForEach(i =>
                {
                    i.HasError = false;
                    i.Error = null;
                    if (i.instName != null && custInstances.Exists(s => (s.Name.ToLower() == i.instName.ToLower())))
                    {
                        i.HasError = true;
                        i.Error = Constants.ERR_INSTANCE_NAME;                                          
                    }
                });

                if (instances.Any(i => i.HasError))
                {
                    var jsonErr = new JavaScriptSerializer().Serialize(instances);
                    jsonResp.Content = new StringContent(jsonErr);
                    jsonResp.StatusCode = HttpStatusCode.InternalServerError;
                    throw new HttpResponseException(jsonResp);
                }
                
                List<Dictionary<string, object>> upsValues = new List<Dictionary<string, object>>();
                upsValues = InstanceMappingRepository.BuildSKUMappingtoUpdate(ref instances);

                var reqConfig = new RequestConfigModel();
                reqConfig.accessToken = accessToken;
                reqConfig.objectName = Constants.OBJ_CPQ_LINEITEM;
                jsonResp = Utilities.Upsert(upsValues, reqConfig, false);

                var jsonMsg = new JavaScriptSerializer().Serialize(instances);
                jsonResp.Content = new StringContent(jsonMsg);
                return jsonResp;
            }

            catch(HttpResponseException hEx)
            {
                if (!instances.Any(i => i.HasError))
                {
                    instances[0].Error = Constants.ERR_INSTANCE_GENERIC;
                }
                
                var jsonErr = new JavaScriptSerializer().Serialize(instances);
                jsonResp.Content = new StringContent(jsonErr);
                jsonResp.StatusCode = HttpStatusCode.InternalServerError;
                throw new HttpResponseException(jsonResp);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // PUT: api/InstanceMap/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/InstanceMap/5
        public void Delete(int id)
        {
        }
    }
}
