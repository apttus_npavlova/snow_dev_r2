﻿/****************************************************************************************
@Name: InvoiceScheduleBuilderRepository.cs
@Author: Maunish P. Shah
@CreateDate: 12 Oct 2017
@Description: Generates the Invoice Schedules 
@UsedBy:

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/
using Apttus.DataAccess.Common.Enums;
using Apttus.DataAccess.Common.Model;
using Apttus.SNowPS.Common;
using Apttus.SNowPS.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;

namespace Apttus.SNowPS.Repository
{
    public class InvoiceScheduleBuilderRepository
    {
        public HttpResponseMessage GenerateInvSchedule(string QuoteId, string accessToken)
        {
            try
            {
                string quoteName = string.Empty;
                DateTime quoteStartDate = new DateTime();
                DateTime quoteEndDate = new DateTime();
                int quoteStartYear = 0;
                int quoteEndYear = 0;
                int netPaymentDueIn = 0;
                string paymentTermText = string.Empty;
                int quoteTerm = 0;
                int quotetenure = 0;
                DataTable invoiceScheduleTable = new DataTable();

                //Build AQL Query to fetch Quote,Quote Line Item
                var query = new Query(Constants.OBJ_QUOTE);
                query.AddColumns(Constants.OBJ_FIELDORDER_INVSCH.Split(Constants.CHAR_COMMA));
                Expression exp = new Expression(ExpressionOperator.AND);
                exp.AddCondition(new Condition(Constants.FIELD_ID, FilterOperator.Equal, QuoteId));

                query.SetCriteria(exp);

                Join quoteLineItem = new Join(Constants.OBJ_QUOTE, Constants.OBJ_CPQ_QUOTELINEITEM, Constants.FIELD_ID, Constants.FIELD_QUOTEID, JoinType.LEFT);
                quoteLineItem.EntityAlias = Constants.OBJ_CPQ_QUOTELINEITEM;
                query.AddJoin(quoteLineItem);

                var jsonQuery = query.Serialize();

                var reqConfig = new RequestConfigModel();
                reqConfig.accessToken = accessToken;
                reqConfig.searchType = SearchType.AQL;
                reqConfig.objectName = Constants.OBJ_QUOTE;

                var response = Utilities.Search(jsonQuery, reqConfig);
                
                List<QuoteQuoteLineItem> QuoteLineItem = new List<QuoteQuoteLineItem>();
                if (response != null && response.IsSuccessStatusCode)
                {
                    var responseString = JObject.Parse(response.Content.ReadAsStringAsync().Result).SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES).ToString();
                    
                    List<QuoteQuoteLineDetails> QuoteLineList = new List<QuoteQuoteLineDetails>();
                    QuoteLineList = JsonConvert.DeserializeObject<List<QuoteQuoteLineDetails>>(responseString);
                    
                    
                    if (QuoteLineList != null && QuoteLineList.Count > 0)
                    {
                        // Set the quotename, quoteid, quote startdate, quote enddate values to the variables.                                                
                        quoteName = QuoteLineList[0].Name;
                        quoteStartDate = (DateTime) QuoteLineList[0].expectedStartDate;
                        quoteEndDate = (DateTime) QuoteLineList[0].expectedEndDate;
                        quoteStartYear = quoteStartDate.Year;
                        quoteEndYear = quoteEndDate.Year;
                        paymentTermText = QuoteLineList[0].paymentTerm.Value;
                        netPaymentDueIn = Convert.ToInt32(Regex.Replace(paymentTermText, "[^0-9]+", string.Empty));
                        quotetenure = (Convert.ToInt32(quoteEndDate.Year) - Convert.ToInt32(quoteStartDate.Year));
                        //quoteTerm = QuoteLineList[0].paymentTerm;

                        if (quoteTerm == 0)
                            quoteTerm = ((quoteEndDate - quoteStartDate).Days / 12);

                        invoiceScheduleTable.Columns.Add("ScheduleNumber");
                        invoiceScheduleTable.Columns.Add("ScheduleName");
                        invoiceScheduleTable.Columns.Add("RangeEnd");
                        invoiceScheduleTable.Columns.Add("RangeEndDate");
                        invoiceScheduleTable.Columns.Add("ScheduleDueDate");
                        invoiceScheduleTable.Columns.Add("Amount");

                        QuoteQuoteLineItem qlineitem;

                        // Fill the first table of the QLI
                        for (int i = 0; i < QuoteLineList.Count; i++)
                        {
                            qlineitem = new QuoteQuoteLineItem();
                            qlineitem.Id = QuoteLineList[i].cpq_QuoteLineItem.Id;
                            qlineitem.Name = QuoteLineList[i].cpq_QuoteLineItem.Name;
                            qlineitem.NetPrice = QuoteLineList[i].cpq_QuoteLineItem.NetPrice;
                            qlineitem.StartDate = QuoteLineList[i].cpq_QuoteLineItem.StartDate;
                            qlineitem.EndDate = QuoteLineList[i].cpq_QuoteLineItem.EndDate;
                            QuoteLineItem.Add(qlineitem);
                        }
                    }
                }

                // Calculate the second table of InvoiceSchedule
                if (quotetenure > 0)
                {
                    double sumOfSubAmount = 0;
                    int prevRangeEnd = 0;

                    for (int tenure = 0; tenure < quotetenure; tenure++)
                    {
                        sumOfSubAmount = 0;
                        DataRow scheduleRow = invoiceScheduleTable.NewRow();
                        scheduleRow["ScheduleNumber"] = tenure + 1;
                        scheduleRow["ScheduleName"] = "Annual Subscription Fee";

                        if (prevRangeEnd != 0)
                        {
                            if (prevRangeEnd <= (quoteEndYear - 1))
                            {
                                if (quoteStartYear != quoteEndYear)
                                {
                                    scheduleRow["RangeEnd"] = prevRangeEnd + 1;
                                    scheduleRow["RangeEndDate"] = quoteStartDate.AddYears(tenure + 1).AddDays(-1).ToShortDateString();
                                    scheduleRow["ScheduleDueDate"] = (scheduleRow["RangeEndDate"].ConvertToDate()).Value.AddDays((-1)*netPaymentDueIn).AddDays(-1);
                                }
                                else
                                {
                                    scheduleRow["RangeEnd"] = prevRangeEnd;
                                    scheduleRow["RangeEndDate"] = quoteStartDate.AddYears(tenure + 1).AddDays(-1).ToShortDateString();
                                }
                            }
                        }

                        else
                        {
                            scheduleRow["RangeEnd"] = quoteStartYear + 1;
                            scheduleRow["RangeEndDate"] = quoteStartDate.AddYears(tenure + 1).AddDays(-1).ToShortDateString();
                            scheduleRow["ScheduleDueDate"] = "Upon Signature";
                        }
                                                                        
                        for (int qli = 0; qli < QuoteLineItem.Count(); qli++)
                        {                            
                            if (QuoteLineItem[qli].StartDate != null && QuoteLineItem[qli].EndDate != null && QuoteLineItem[qli].NetPrice > 0 && (Convert.ToDateTime(QuoteLineItem[qli].EndDate).Year >= scheduleRow["RangeEnd"].ConvertToInteger()) && (Convert.ToDateTime(QuoteLineItem[qli].StartDate) < scheduleRow["RangeEndDate"].ConvertToDate()) && (Convert.ToDateTime(QuoteLineItem[qli].EndDate).Year != Convert.ToDateTime(QuoteLineItem[qli].StartDate).Year))
                            {
                                sumOfSubAmount = sumOfSubAmount + (Convert.ToDouble(QuoteLineItem[qli].NetPrice) / (Convert.ToDateTime(QuoteLineItem[qli].EndDate).Year - Convert.ToDateTime(QuoteLineItem[qli].StartDate).Year));
                            }

                            else if (Convert.ToDateTime(QuoteLineItem[qli].EndDate).Year == Convert.ToDateTime(QuoteLineItem[qli].StartDate).Year)
                            {
                                // use case 3 task. this is not yet ready.
                            }
                        }
                                                
                        scheduleRow["Amount"] = sumOfSubAmount;
                        invoiceScheduleTable.Rows.Add(scheduleRow);
                        prevRangeEnd = scheduleRow["RangeEnd"].ConvertToInteger();
                    }
                }

                if (quotetenure > 0)
                {
                    double sumOfSubAmountSameYear = 0;
                    int prevRangeEndSameYear = 0;
                    int months = 0;
                    int days = 0;

                    
                    for (int qli = 0; qli < QuoteLineItem.Count(); qli++)
                    {
                        if (QuoteLineItem[qli].StartDate != null && QuoteLineItem[qli].EndDate != null && QuoteLineItem[qli].NetPrice > 0 && (Convert.ToDateTime(QuoteLineItem[qli].EndDate).Year == Convert.ToDateTime(QuoteLineItem[qli].StartDate).Year))
                        {
                            DataRow scheduleRow = invoiceScheduleTable.NewRow();
                            scheduleRow["ScheduleNumber"] = 0;                            
                            months = DateTimeHelper.CompareDates(Convert.ToDateTime(QuoteLineItem[qli].EndDate), Convert.ToDateTime(QuoteLineItem[qli].StartDate)).Months;
                            days = Convert.ToInt32(Convert.ToDateTime(QuoteLineItem[qli].EndDate).Day);
                            scheduleRow["ScheduleName"] = "" + months + " Months" + ", " + days + " Days Subscription Fee";
                            
                            sumOfSubAmountSameYear = sumOfSubAmountSameYear + (Convert.ToDouble(QuoteLineItem[qli].NetPrice));
                            prevRangeEndSameYear = Convert.ToDateTime(QuoteLineItem[qli].EndDate).Year;
                            scheduleRow["Amount"] = sumOfSubAmountSameYear;
                            scheduleRow["RangeEnd"] = prevRangeEndSameYear;
                            scheduleRow["RangeEndDate"] = Convert.ToDateTime(QuoteLineItem[qli].EndDate).AddYears(1).AddDays(-1).ToShortDateString();
                            scheduleRow["ScheduleDueDate"] = (scheduleRow["RangeEndDate"].ConvertToDate()).Value.AddDays((-1) * netPaymentDueIn).AddDays(-1);
                            invoiceScheduleTable.Rows.Add(scheduleRow);
                        }
                    }
                }

                var Rows = (from row in invoiceScheduleTable.AsEnumerable()
                            orderby row["RangeEnd"] ascending
                            select row);
                invoiceScheduleTable = Rows.AsDataView().ToTable();

                List<QuoteLineItemInvSchedule> resultQLIInvoiceSchedule = new List<QuoteLineItemInvSchedule>();
                for (int invsch = 0; invsch < invoiceScheduleTable.Rows.Count; invsch++)
                {
                    QuoteLineItemInvSchedule resultQLIInvSch = new QuoteLineItemInvSchedule();
                    resultQLIInvSch.invoiceScheduleNumber = Convert.ToInt32(invoiceScheduleTable.Rows[invsch]["ScheduleNumber"]);
                    resultQLIInvSch.invoiceScheduleName = Convert.ToString(invoiceScheduleTable.Rows[invsch]["ScheduleName"]);
                    resultQLIInvSch.amountToBeCharged = Convert.ToDecimal(invoiceScheduleTable.Rows[invsch]["Amount"]);
                    resultQLIInvSch.dueDate = Convert.ToString(invoiceScheduleTable.Rows[invsch]["ScheduleDueDate"]);
                    resultQLIInvoiceSchedule.Add(resultQLIInvSch);
                }
                
                var serializedObj = JsonConvert.SerializeObject(resultQLIInvoiceSchedule);
                var deserializedObj = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(serializedObj.ToString());
                var contentSerialized = JsonConvert.SerializeObject(deserializedObj);


                //check if there are existing invoice schedules available for the quote. If yes, then delete them.

                //now add the newly created invoice schedules.


                ////return response
                var resp = new HttpResponseMessage
                {
                    Content = new StringContent(contentSerialized, System.Text.Encoding.UTF8, "application/json"),                    
                    StatusCode = System.Net.HttpStatusCode.OK
                };
                return resp;
            }
            catch (Exception ex)
            {
                //TODO
                var resp = new HttpResponseMessage
                {                    
                    Content = new StringContent("Message :" + ex.Message + " " + "StackStrace :" + ex.StackTrace, System.Text.Encoding.UTF8, "application/json"),
                    StatusCode = System.Net.HttpStatusCode.BadRequest
                };
                return resp;
            }
        }

        
    }
}
