﻿using Apttus.SNowPS.Common;
using Apttus.SNowPS.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apttus.SNowPS.Repository.CLM
{
    public class InvoiceScheduleRepository
    {
        public string AccessToken { get; set; }

        public InvoiceScheduleRepository(string accessToken)
        {
            AccessToken = accessToken;
        }

        public void Update(string quoteId, string agreementId)
        {
            RequestModel request = new RequestModel()
            {
                select = new string[] { InvoiceScheduleObjectConstants.Quote, InvoiceScheduleObjectConstants.Agreement },
                filter = new List<FilterModel>
                {
                    new FilterModel
                    {
                        field = InvoiceScheduleObjectConstants.Quote,
                        @operator = "Equal",
                        value = new string[] {  quoteId }
                    },
                    new FilterModel
                    {
                        field = InvoiceScheduleObjectConstants.Agreement,
                        @operator = "Equal",
                        value = new string[] { agreementId }
                    }
                },
                logicalExpression = "(1 OR 2)"
            };

            var query = JsonConvert.SerializeObject(request);

            var reqConfig = new RequestConfigModel();
            reqConfig.accessToken = AccessToken;
            reqConfig.objectName = Constants.OBJ_EXT_INVOICESCHEDULE;

            // Get the agreement with column Sales Ops Rep
            var response = Utilities.GetSearch(query, reqConfig);

            var responseContent = response.Content.ReadAsStringAsync().Result;
            dynamic jObj = JObject.Parse(responseContent);
            var invoiceJson = jObj.SerializedResultEntities;

            List<Dictionary<string, object>> invoiceSchedulesToUpdate = new List<Dictionary<string, object>>();

            foreach (var invoiceSchedule in invoiceJson)
            {
                string responseQuoteId = invoiceSchedule.ext_Quote != null ? 
                    invoiceSchedule.ext_Quote.Id : string.Empty;
                string responseAgreementId = invoiceSchedule.ext_agreement != null ? 
                    invoiceSchedule.ext_agreement.Id : string.Empty;

                Dictionary<string, object> invoiceScheduleToUpdate = new Dictionary<string, object>();
                invoiceScheduleToUpdate["Id"] = invoiceSchedule.Id;
                if (quoteId == responseQuoteId && agreementId != responseAgreementId)
                {
                    invoiceScheduleToUpdate["ext_Agreement"] = new
                    {
                        Id = agreementId
                    };

                    invoiceSchedulesToUpdate.Add(invoiceScheduleToUpdate);
                }
                else if (quoteId != responseQuoteId && agreementId == responseAgreementId)
                {
                    invoiceScheduleToUpdate["ext_Agreement"] = null;

                    invoiceSchedulesToUpdate.Add(invoiceScheduleToUpdate);
                }
            }

            Utilities.Update(invoiceSchedulesToUpdate, new RequestConfigModel
            {
                accessToken = AccessToken,
                objectName = Constants.OBJ_EXT_INVOICESCHEDULE
            });
        }
    }
}
