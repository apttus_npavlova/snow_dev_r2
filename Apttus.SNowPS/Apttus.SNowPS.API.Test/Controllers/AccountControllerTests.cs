﻿/****************************************************************************************
@Name: AccountControllerTests.cs
@Author: Akash Kakdiya
@CreateDate: 7 Oct 2017
@Description: Utilty for Unit Test Project
@UsedBy: This will be used by Account for unit test.

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/

using Apttus.SNowPS.API.Controllers;
using Apttus.SNowPS.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;
using System.Web.Http.Hosting;

namespace Apttus.SNowPS.API.Test.Controllers
{
    [TestClass()]
    public class AccountControllerTests
    {
        [TestMethod()]
        public void GetTest()
        {
            string uri = "https://servicenow-dev3.apttuscloud.com/api/generic/v1/search/advance/crm_account?query={'select':['Name','ExternalId','AccountNumber','AccountSource','ParentId']}";
            var controller = new AccountController();
            controller.Request = new HttpRequestMessage();
            controller.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());
            controller.Request.RequestUri = new Uri(uri);
            controller.Request.Headers.Add("Authorization", Utilities.GetAuthToken());
            var test = controller.Get();
            if (!test.IsSuccessStatusCode)
            {
                Assert.Fail();
            }
        }

		[TestMethod()]
		public void PostTest()
		{
			try
			{
				var controller = new AccountController();
				controller.Request = new HttpRequestMessage();
				controller.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());
				controller.Request.Headers.Add("Authorization", Utilities.GetAuthToken());
				List<Dictionary<string, object>> jsonContent = GetRequestDictionary("Account", "AccountUpsert");
				var test = controller.UpsertAccount(jsonContent);
				if (!test.IsSuccessStatusCode)
				{
					Assert.IsTrue(false, "Fail while asserting");
				}
			}
			catch (AssertFailedException ex)
			{
				throw new Exception(ex.Message);
			}

		}


		//[TestMethod()]
		//public void AccountSyncTest()
		//{
		//    string uri = "https://servicenow-dev3.apttuscloud.com/api/generic/v1/search/advance/crm_account?query={'select':['Name','ExternalId','AccountNumber','AccountSource','ParentId']}";
		//    var controller = new AccountController();
		//    controller.Request = new HttpRequestMessage();
		//    controller.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());
		//    controller.Request.RequestUri = new Uri(uri);
		//    controller.Request.Headers.Add("Authorization", Utilities.GetAuthToken());
		//    List<Dictionary<string, object>> dictContent = GetRequestDictionary("Account", "AccountSync"); ;
		//    var test = controller.AccountSync(dictContent);
		//    if (!test.IsSuccessStatusCode)
		//    {
		//        Assert.Fail();
		//    }
		//}

		public List<Dictionary<string, object>> GetRequestDictionary(string moduleName, string requestJsonFileName)
        {
            List<Dictionary<string, object>> jsonContent = new List<Dictionary<string, object>>(); 
            var absolteFilePath = Assembly.GetExecutingAssembly().Location.Substring(0, Assembly.GetExecutingAssembly().Location.IndexOf("bin"));
            var jsonFileUrl = absolteFilePath + "Json/" + moduleName + "/" + requestJsonFileName + ".json";
            using (StreamReader file = File.OpenText(Convert.ToString(jsonFileUrl)))
            using (JsonTextReader reader = new JsonTextReader(file))
            {
                var Content = ((JObject)JToken.ReadFrom(reader)).ToObject<Dictionary<string, object>>();
                jsonContent.Add(Content);
            }
            return jsonContent;
        }


    }
}